import 'package:dynamic_color/dynamic_color.dart';
import 'package:flutter/material.dart';
import 'package:knitting_counter/start/start_page.dart';
import 'package:knitting_counter/utils/persistence.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final preferences = await SharedPreferences.getInstance();
  runApp(MyApp(preferences: preferences));
}

class MyApp extends StatelessWidget {
  const MyApp({
    super.key,
    required this.preferences,
  });

  final SharedPreferences preferences;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: Persistence(
            preferences: preferences,
          ),
        ),
      ],
      child: MaterialApp(
        title: 'Beanie',
        theme: ThemeData(
          brightness: Brightness.light,
          colorSchemeSeed: const Color.fromARGB(255, 62, 191, 107),
          useMaterial3: true,
        ),
        darkTheme: ThemeData(
          brightness: Brightness.dark,
          colorSchemeSeed: const Color.fromARGB(255, 62, 191, 107),
          useMaterial3: true,
        ),
        home: const StartPage(),
      ),
    );
  }
}
