import 'package:freezed_annotation/freezed_annotation.dart';

part 'counter.freezed.dart';
part 'counter.g.dart';

@unfreezed
class ProjectCounter with _$ProjectCounter {
  factory ProjectCounter({
    @Default('') String name,
    @Default(0) int value,
    int? limit,
  }) = _ProjectCounter;

  factory ProjectCounter.fromJson(Map<String, Object?> json) =>
      _$ProjectCounterFromJson(json);
}
