import 'dart:async';

import 'package:knitting_counter/project/project.dart';
import 'package:knitting_counter/utils/formatting.dart';
import 'package:knitting_counter/utils/persistence.dart';
import 'package:simple_model_state/simple_model_state.dart';

class ProjectPageModel extends BaseModel {
  final Project _project;
  Project get project => _project;

  final Persistence _persistence;

  Timer? _ticker;
  DateTime? _timerStart;
  bool get timerStarted => _timerStart != null;
  bool get timerNotStarted => _timerStart == null;

  ProjectPageModel({
    required Project project,
    required Persistence persistence,
  })  : _project = project,
        _persistence = persistence;

  Duration get talliedTime {
    if (timerStarted) {
      return _project.timeSpent + DateTime.now().difference(_timerStart!);
    } else {
      return _project.timeSpent;
    }
  }

  Future<void> addCounter(int counterIndex) async {
    final counter = _project.counters[counterIndex];
    if (counter.limit == null || counter.value < counter.limit!) {
      _project.counters[counterIndex] = counter.copyWith(
        value: counter.value + 1,
      );
    }
    await _persistence.saveProject(_project);
    notifyListeners();
  }

  void subtractCounter(int counterIndex) {
    final counter = _project.counters[counterIndex];
    if (counter.value > 0) {
      _project.counters[counterIndex] = counter.copyWith(
        value: counter.value - 1,
      );
    }
    _persistence.saveProject(_project);
    notifyListeners();
  }

  void startTimer() {
    if (timerNotStarted) {
      _timerStart = DateTime.now();
      _ticker = Timer.periodic(
        const Duration(milliseconds: 100),
        (_) => notifyListeners(),
      );
      notifyListeners();
    }
  }

  Future<void> stopTimer() async {
    if (timerStarted) {
      final timeSpent = DateTime.now().difference(_timerStart!);
      _timerStart = null;
      _project.timeSpent += timeSpent;
      await _persistence.saveProject(_project);
      _ticker!.cancel();
      notifyListeners();
    }
  }

  @override
  void dispose() {
    stopTimer();
    super.dispose();
  }
}
