import 'package:freezed_annotation/freezed_annotation.dart';

part 'project_instructions.freezed.dart';
part 'project_instructions.g.dart';

@freezed
class ProjectInstructions with _$ProjectInstructions {
  const factory ProjectInstructions.none() = _ProjectInstructionsNone;

  factory ProjectInstructions.pdf({
    required String filename,
  }) = _ProjectInstructionsPdf;

  factory ProjectInstructions.fromJson(Map<String, Object?> json) =>
      _$ProjectInstructionsFromJson(json);
}
