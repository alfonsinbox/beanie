import 'package:flutter/material.dart';
import 'package:knitting_counter/project/counter.dart';

class CreateCounterDialog extends StatefulWidget {
  final String? defaultName;

  const CreateCounterDialog({super.key, this.defaultName});

  @override
  State<CreateCounterDialog> createState() => _CreateCounterDialogState();
}

class _CreateCounterDialogState extends State<CreateCounterDialog> {
  var counter = ProjectCounter();

  final limitController = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (widget.defaultName != null) {
      counter = counter.copyWith(name: widget.defaultName!);
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Skapa räknare'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextFormField(
            autofocus: true,
            initialValue: counter.name,
            onChanged: (value) {
              counter = counter.copyWith(name: value);
            },
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Namn',
            ),
          ),
          const SizedBox(height: 24),
          if (counter.limit == null)
            OutlinedButton(
              onPressed: () {
                setState(() {
                  counter = counter.copyWith(limit: 1);
                  limitController.text = counter.limit.toString();
                });
              },
              child: const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'Sätt gräns',
                  maxLines: 1,
                  softWrap: false,
                  overflow: TextOverflow.fade,
                ),
              ),
            ),
          if (counter.limit != null)
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: IconButton(
                    onPressed: () {
                      setState(() {
                        counter = counter.copyWith(limit: counter.limit! + 1);
                        limitController.text = counter.limit.toString();
                      });
                    },
                    icon: const Icon(Icons.add),
                  ),
                ),
                Expanded(
                  child: TextFormField(
                    onChanged: (limitText) {
                      final intLimit = int.tryParse(limitText);
                      if (intLimit != null && intLimit > 0) {
                        counter = counter.copyWith(limit: intLimit);
                      }
                    },
                    decoration: const InputDecoration(
                      labelText: 'Gräns',
                    ),
                    controller: limitController,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: IconButton(
                    onPressed: counter.limit! > 1
                        ? () {
                            setState(() {
                              counter =
                                  counter.copyWith(limit: counter.limit! - 1);
                              limitController.text = counter.limit.toString();
                            });
                          }
                        : null,
                    icon: const Icon(Icons.remove),
                  ),
                ),
              ],
            ),
        ],
      ),
      actions: [
        TextButton(
          onPressed: () => Navigator.of(context).pop(),
          child: const Text('Avbryt'),
        ),
        TextButton(
          onPressed: counter.name.isNotEmpty
              ? () => Navigator.of(context).pop(counter)
              : null,
          child: const Text('Spara'),
        ),
      ],
    );
  }
}
