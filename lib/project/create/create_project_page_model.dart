import 'package:knitting_counter/project/counter.dart';
import 'package:knitting_counter/project/create/create_project.dart';
import 'package:knitting_counter/project/project.dart';
import 'package:knitting_counter/project/project_instructions.dart';
import 'package:knitting_counter/utils/persistence.dart';
import 'package:simple_model_state/simple_model_state.dart';
import 'package:uuid/uuid.dart';

class CreateProjectPageModel extends BaseModel {
  CreateProject _project = const CreateProject();

  final Persistence _persistence;

  CreateProjectPageModel({
    required Persistence persistence,
  }) : _persistence = persistence;

  ProjectInstructions get instructions => _project.instructions;
  List<ProjectCounter> get counters => _project.counters;

  void setName(String name) {
    _project = _project.copyWith(name: name);
    notifyListeners();
  }

  void setInstructions(ProjectInstructions instructions) {
    _project = _project.copyWith(instructions: instructions);
    notifyListeners();
  }

  bool checkProject() {
    return _project.name != null;
  }

  Future<void> save() async {
    await _persistence.saveProject(Project(
      id: const Uuid().v4().toString(),
      name: _project.name!,
      createdAt: DateTime.now(),
      counters: _project.counters,
      instructions: _project.instructions,
    ));
  }

  void addCounter(ProjectCounter counter) {
    _project = _project.copyWith(counters: _project.counters + [counter]);
    notifyListeners();
  }

  void deleteCounter(int index) {
    _project.counters.removeAt(index);
    notifyListeners();
  }
}
