import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:knitting_counter/project/counter.dart';
import 'package:knitting_counter/project/create/create_counter_dialog.dart';
import 'package:knitting_counter/project/create/create_project_page_model.dart';
import 'package:knitting_counter/project/create/select_instructions_page.dart';
import 'package:knitting_counter/project/project_instructions.dart';
import 'package:provider/provider.dart';
import 'package:simple_model_state/simple_model_state.dart';

class CreateProjectPage extends StatefulWidget {
  const CreateProjectPage({super.key});

  @override
  State<CreateProjectPage> createState() => _CreateProjectPageState();
}

class _CreateProjectPageState extends State<CreateProjectPage> {
  late final CreateProjectPageModel model;

  @override
  void initState() {
    super.initState();
    model = CreateProjectPageModel(
      persistence: Provider.of(context, listen: false),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      model: model,
      builder: (context, model, child) {
        return Scaffold(
          appBar: AppBar(
            title: const Text('Skapa projekt'),
          ),
          body: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextField(
                            onChanged: model.setName,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Namn',
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: OutlinedButton(
                                  onPressed: () async {
                                    final selected = await Navigator.of(context)
                                        .push<String>(
                                      MaterialPageRoute(
                                        builder: (context) {
                                          return const SelectInstructionsPage();
                                        },
                                      ),
                                    );
                                    if (selected != null) {
                                      model.setInstructions(
                                        ProjectInstructions.pdf(
                                          filename: selected,
                                        ),
                                      );
                                    }
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      model.instructions.when(
                                        none: () => 'Välj mall',
                                        pdf: (filename) =>
                                            filename.split('/').last,
                                      ),
                                      maxLines: 1,
                                      softWrap: false,
                                      overflow: TextOverflow.fade,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        for (final entry in model.counters.asMap().entries)
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: entry.value.limit == null
                                      ? Text(entry.value.name)
                                      : Text(
                                          '${entry.value.name} (Gräns ${entry.value.limit!})',
                                        ),
                                ),
                                IconButton(
                                  onPressed: () =>
                                      model.deleteCounter(entry.key),
                                  icon: const Icon(Icons.delete),
                                )
                              ],
                            ),
                          ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: OutlinedButton(
                                  onPressed: () async {
                                    final counter = await showDialog(
                                      context: context,
                                      builder: (context) {
                                        return CreateCounterDialog(
                                          defaultName:
                                              'Räknare ${model.counters.length + 1}',
                                        );
                                      },
                                    );
                                    if (counter != null) {
                                      model.addCounter(counter);
                                    }
                                  },
                                  child: const Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text(
                                      'Lägg till räknare',
                                      maxLines: 1,
                                      softWrap: false,
                                      overflow: TextOverflow.fade,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                OutlinedButton(
                  onPressed: model.checkProject()
                      ? () async {
                          await model.save();
                          if (!mounted) return;
                          Navigator.of(context).pop();
                        }
                      : null,
                  child: const Text('Spara'),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
