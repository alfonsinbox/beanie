import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

class SelectInstructionsPage extends StatefulWidget {
  const SelectInstructionsPage({super.key});

  @override
  State<SelectInstructionsPage> createState() => SelectInstructionsPageState();
}

class SelectInstructionsPageState extends State<SelectInstructionsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Välj här vettja'),
      ),
      body: GridView(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
        ),
        padding: const EdgeInsets.all(8.0),
        children: [
          Card(
            clipBehavior: Clip.antiAlias,
            child: InkWell(
              onTap: () async {
                final result = await FilePicker.platform.pickFiles(
                  type: FileType.custom,
                  allowedExtensions: ['pdf'],
                );
                if (result?.files.single.path != null) {
                  final selectedFile = File(result!.files.single.path!);
                  if (!mounted) return;
                  Navigator.pop(context, selectedFile.path);
                } else {
                  // User canceled the picker
                }
              },
              child: const Center(
                child: Text('PDF'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
