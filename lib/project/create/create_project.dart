import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:knitting_counter/project/counter.dart';
import 'package:knitting_counter/project/project_instructions.dart';

part 'create_project.freezed.dart';

@Freezed(makeCollectionsUnmodifiable: false)
class CreateProject with _$CreateProject {
  const factory CreateProject({
    String? name,
    @Default(ProjectInstructions.none()) ProjectInstructions instructions,
    @Default([]) List<ProjectCounter> counters,
  }) = _CreateProject;
}
