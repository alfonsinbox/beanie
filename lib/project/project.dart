import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:knitting_counter/project/counter.dart';
import 'package:knitting_counter/project/create/create_project.dart';
import 'package:knitting_counter/project/project_instructions.dart';
import 'package:uuid/uuid.dart';

part 'project.freezed.dart';
part 'project.g.dart';

@unfreezed
class Project with _$Project {
  factory Project({
    required String id,
    required String name,
    required DateTime createdAt,
    @Default(Duration.zero) Duration timeSpent,
    @Default([]) List<ProjectCounter> counters,
    @Default(ProjectInstructions.none()) ProjectInstructions instructions,
  }) = _Project;

  factory Project.fromJson(Map<String, Object?> json) =>
      _$ProjectFromJson(json);
}
