import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:knitting_counter/project/project.dart';
import 'package:knitting_counter/project/project_page_model.dart';
import 'package:knitting_counter/utils/formatting.dart';
import 'package:provider/provider.dart';
import 'package:simple_model_state/simple_model_state.dart';

class ProjectPage extends StatefulWidget {
  final Project project;

  const ProjectPage({
    super.key,
    required this.project,
  });

  @override
  State<ProjectPage> createState() => _ProjectPageState();
}

class _ProjectPageState extends State<ProjectPage> {
  late final ProjectPageModel model;

  @override
  void initState() {
    super.initState();
    model = ProjectPageModel(
      project: widget.project,
      persistence: Provider.of(context, listen: false),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      model: model,
      builder: (context, value, child) {
        return Scaffold(
          appBar: AppBar(
            title: Text(model.project.name),
          ),
          body: Column(
            children: [
              AnimatedContainer(
                duration: const Duration(milliseconds: 200),
                color: model.timerStarted
                    ? const Color.fromARGB(255, 100, 183, 102)
                    : const Color.fromARGB(255, 141, 63, 57),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: InkWell(
                    onTap:
                        model.timerStarted ? model.stopTimer : model.startTimer,
                    child: Material(
                      borderRadius: BorderRadius.circular(8),
                      elevation: 4,
                      shadowColor: Colors.black,
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Row(
                          children: [
                            Icon(
                              model.timerStarted
                                  ? Icons.stop
                                  : Icons.play_arrow,
                            ),
                            const SizedBox(width: 16),
                            Text(formatDuration(model.talliedTime)),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              for (final counterEntry in model.project.counters.asMap().entries)
                IntrinsicHeight(
                  child: Stack(
                    children: [
                      FractionallySizedBox(
                        widthFactor: counterEntry.value.limit != null
                            ? counterEntry.value.value /
                                counterEntry.value.limit!
                            : 0,
                        child: Opacity(
                          opacity: 0.1,
                          child: Container(
                            color: Colors.black,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(24.0),
                            child: counterEntry.value.limit != null
                                ? CircularProgressIndicator(
                                    value: counterEntry.value.value /
                                        counterEntry.value.limit!,
                                  )
                                : Opacity(
                                    opacity: 0.1,
                                    child: CircularProgressIndicator(
                                      value: 1.0,
                                      strokeWidth:
                                          const CircularProgressIndicator()
                                                  .strokeWidth /
                                              2.0,
                                    ),
                                  ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                counterEntry.value.name,
                                style: Theme.of(context).textTheme.titleMedium,
                                maxLines: 1,
                                overflow: TextOverflow.fade,
                                softWrap: false,
                              ),
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              model.addCounter(counterEntry.key);
                            },
                            icon: const Icon(
                              Icons.add,
                              size: 24.0,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              '${counterEntry.value.value}',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleLarge
                                  ?.copyWith(fontWeight: FontWeight.bold),
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              model.subtractCounter(counterEntry.key);
                            },
                            icon: const Icon(
                              Icons.remove,
                              size: 24.0,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
            ],
          ) /* widget.project.file != null
          ? PDFView(filePath: widget.project.file)
          : Center(
              child: TextButton(
                child: Text('boopsie'),
                onPressed: () async {
                  final result = await FilePicker.platform.pickFiles(
                    type: FileType.custom,
                    allowedExtensions: ['pdf'],
                  );
                  if (result?.files.single.path != null) {
                    final selectedFile = File(result!.files.single.path!);
                    setState(() => file = selectedFile.path);
                  } else {
                    // User canceled the picker
                  }
                },
              ),
            ) */
          ,
        );
      },
    );
  }
}
