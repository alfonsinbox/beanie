import 'package:flutter/material.dart';
import 'package:knitting_counter/project/create/create_project_page.dart';
import 'package:knitting_counter/project/project.dart';
import 'package:knitting_counter/project/project_page.dart';
import 'package:knitting_counter/start/start_page_model.dart';
import 'package:knitting_counter/utils/formatting.dart';
import 'package:provider/provider.dart';
import 'package:simple_model_state/simple_model_state.dart';

class StartPage extends StatefulWidget {
  const StartPage({super.key});

  @override
  State<StartPage> createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> {
  late final StartPageModel model;

  @override
  void initState() {
    super.initState();
    model = StartPageModel(
      persistence: Provider.of(context, listen: false),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      model: model,
      builder: (context, model, _) {
        return Scaffold(
          body: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(16).copyWith(top: 48),
                    child: Row(
                      children: [
                        Text(
                          'Beanie',
                          style: Theme.of(context)
                              .textTheme
                              .headline5
                              ?.copyWith(
                                fontFamily: 'Source Serif Pro',
                                fontWeight: FontWeight.bold,
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .headline1
                                    ?.fontSize,
                                color: Theme.of(context).colorScheme.onSurface,
                              ),
                        ),
                      ],
                    ),
                  ),
                  for (final project in model.projects)
                    StartPageProjectItem(
                      project: project,
                      onRemove: model.removeProject,
                    ),
                  // Just some bottom padding
                  const Padding(
                    padding: EdgeInsets.all(16.0),
                    child: SizedBox(
                      height: 48,
                    ),
                  ),
                ],
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            child: const Icon(Icons.add),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return const CreateProjectPage();
                  },
                ),
              );
            },
          ),
        );
      },
    );
  }
}

class StartPageProjectItem extends StatelessWidget {
  const StartPageProjectItem({
    Key? key,
    required this.project,
    required this.onRemove,
  }) : super(key: key);

  final Project project;
  final Function(Project) onRemove;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      child: Card(
        clipBehavior: Clip.antiAlias,
        child: InkWell(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) {
                  return ProjectPage(
                    project: project,
                  );
                },
              ),
            );
          },
          child: Padding(
            padding: const EdgeInsets.all(32.0),
            child: Row(
              children: [
                if (project.counters.isNotEmpty)
                  if (project.counters.first.limit != null)
                    CircularProgressIndicator(
                      value: project.counters.first.value /
                          project.counters.first.limit!,
                      backgroundColor: Colors.black.withOpacity(0.1),
                    ),
                if (project.counters.isNotEmpty)
                  if (project.counters.first.limit == null)
                    Opacity(
                      opacity: 0.1,
                      child: CircularProgressIndicator(
                        value: 1.0,
                        strokeWidth:
                            const CircularProgressIndicator().strokeWidth / 2.0,
                      ),
                    ),
                const SizedBox(width: 32),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        project.name,
                        style: Theme.of(context).textTheme.titleMedium,
                      ),
                      Text(
                        formatDuration(project.timeSpent),
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ],
                  ),
                ),
                PopupMenuButton(
                  elevation: 8,
                  itemBuilder: (context) {
                    return [
                      PopupMenuItem(
                        onTap: () async {
                          Future<void> removalPrompt(_) async {
                            final remove = await showDialog<bool>(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text("Ta bort ${project.name}?"),
                                  content: const Text(
                                    "Det här går inte att ångra",
                                  ),
                                  actions: [
                                    TextButton(
                                      onPressed: () {
                                        Navigator.pop(context, false);
                                      },
                                      child: const Text("Avbryt"),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        Navigator.pop(context, true);
                                      },
                                      child: const Text("Ta bort"),
                                    ),
                                  ],
                                );
                              },
                            );
                            if (remove ?? false) {
                              onRemove(project);
                            }
                          }

                          WidgetsBinding.instance
                              .addPostFrameCallback(removalPrompt);
                        },
                        child: const Text('Ta bort'),
                      ),
                    ];
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
