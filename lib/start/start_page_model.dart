import 'package:knitting_counter/project/project.dart';
import 'package:knitting_counter/utils/persistence.dart';
import 'package:simple_model_state/simple_model_state.dart';

class StartPageModel extends BaseModel {
  var _projects = <Project>[];

  final Persistence _persistence;

  StartPageModel({
    required Persistence persistence,
  }) : _persistence = persistence {
    _init();
  }

  List<Project> get projects => _projects;

  void _init() {
    print("init start");
    _projects.addAll(_persistence.projects);
    _persistence.addListener(_handlePersistenceChange);
    notifyListeners();
  }

  @override
  void dispose() {
    print("dispose start");
    _persistence.removeListener(_handlePersistenceChange);
    super.dispose();
  }

  void _handlePersistenceChange() {
    print("update start");
    _projects = _persistence.projects;
    notifyListeners();
  }

  Future<void> removeProject(Project project) async {
    await _persistence.removeProject(project);
  }
}
