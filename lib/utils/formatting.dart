String formatDuration(Duration duration) {
  final hours = '${duration.inHours}'.padLeft(2, '0');
  final minutes = '${duration.inMinutes.remainder(60)}'.padLeft(2, '0');
  final seconds = '${duration.inSeconds.remainder(60)}'.padLeft(2, '0');
  return '$hours:$minutes:$seconds';
}

String formatDurationPrecise(Duration duration) {
  final hours = '${duration.inHours}'.padLeft(2, '0');
  final minutes = '${duration.inMinutes.remainder(60)}'.padLeft(2, '0');
  final seconds = '${duration.inSeconds.remainder(60)}'.padLeft(2, '0');
  final millis = '${duration.inMilliseconds.remainder(1000)}'.padLeft(3, '0');
  return '$hours:$minutes:$seconds:$millis';
}
