import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:knitting_counter/project/project.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

class Persistence extends ChangeNotifier {
  static const int _persistenceVersion = 1;
  static const String _projectsKey =
      'net.nittiet.beanie.projects_key_v$_persistenceVersion';
  final SharedPreferences _preferences;

  final List<Project> _projects = [];
  List<Project> get projects => _projects;

  Persistence({
    required SharedPreferences preferences,
  }) : _preferences = preferences {
    _init();
  }

  void _init() async {
    projects.clear();
    _preferences
        .getStringList(_projectsKey)
        ?.map(jsonDecode)
        .map((json) => Project.fromJson(json))
        .forEach(projects.add);
  }

  Future<void> saveProject(Project project) async {
    if (_projects.any((element) => element.id == project.id)) {
      for (final projectEntry in _projects.asMap().entries) {
        if (projectEntry.value.id == project.id) {
          _projects[projectEntry.key] = project;
        }
      }
    } else {
      _projects.add(project);
    }
    await _saveProjectsAndNotify();
  }

  Future<void> removeProject(Project project) async {
    _projects.removeWhere((element) => element.id == project.id);
    await _saveProjectsAndNotify();
  }

  Future<void> _saveProjectsAndNotify() async {
    await _preferences.setStringList(
      _projectsKey,
      _projects.map(jsonEncode).toList(),
    );
    notifyListeners();
  }
}
